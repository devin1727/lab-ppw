from django.contrib import admin
from django.conf.urls import url
from .views import pepew
from .views import pepew1
from .views import pepew2
from .views import registration

urlpatterns = [
 		url(r'^$',pepew,name='pepew'),
 		url(r'^pepew1',pepew1,name = 'pepew1'),
 		url(r'^pepew2',pepew2,name = 'pepew2'),
 		url(r'^registration',registration,name = 'registration'),
 		]